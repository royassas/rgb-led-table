#!/usr/bin/env python
import math, sys
import time, socket
import colorsys
class TimedisplayClass:
    spidev = file("/dev/spidev0.0", "wb")    
    def __init__(self,s):   
        self.pixels = [[[0 for x in range(3)] for x in range(10)] for x in range(20)]
        self.numMatrix = [[0,1,0,0,1,0,0,1,0,1,1,0,1,0,1,1,1,1,0,1,1,1,1,1,0,1,0,0,1,0],[1,0,1,1,1,0,1,0,1,0,0,1,1,0,1,1,0,0,1,0,0,0,0,1,1,0,1,1,0,1],[1,0,1,0,1,0,0,0,1,0,0,1,1,0,1,1,0,0,1,0,0,0,0,1,1,0,1,1,0,1],[1,0,1,0,1,0,0,0,1,0,1,0,1,1,1,1,1,0,1,1,0,0,1,0,0,1,0,0,1,1],[1,0,1,0,1,0,0,1,0,0,0,1,0,0,1,0,0,1,1,0,1,0,1,0,1,0,1,0,0,1],[1,0,1,0,1,0,1,0,0,1,0,1,0,0,1,0,0,1,1,0,1,1,0,0,1,0,1,0,0,1],[1,0,1,0,1,0,1,0,0,1,0,1,0,0,1,0,0,1,1,0,1,1,0,0,1,0,1,0,0,1],[0,1,0,1,1,1,1,1,1,0,1,0,0,0,1,1,1,0,0,1,0,1,0,0,0,1,0,1,1,0]]
        self.brightness = 1.0
        self.running = True
        self.s = s
    def drawsnake(self):
        for row in range(0,20):
            if row%2==0:
                for pixel in range(0,10):
                    for color in range(0,3):
                        c=int(self.pixels[row][pixel][color]*self.brightness)
                        self.spidev.write(chr(c & 0xFF))
            else:
                for pixel in range(9,-1,-1):
                    for color in range(0,3):
                        c=int(self.pixels[row][pixel][color]*self.brightness)
                        self.spidev.write(chr(c & 0xFF))            
        self.spidev.flush()
        time.sleep(0.001)
    def timeStart(self):
        print("Time started")
        while self.running:
            try:
                data = self.s.recv(1024)
                if data=="AbOrTTrObA":
                    self.running=False
            except: 
                pass                         
            timestring = time.strftime("%H%M")
            zahleins = int(timestring[0])
            for x in range (1,9):
                for y in range (1,4):
                    if self.numMatrix[(x-1)][(zahleins*3)+(y-1)]==1:
                        self.pixels[y][9-x]=[0,0,255]
                    else:
                        self.pixels[y][9-x]=[0,0,0]
            zahlzwei = int(timestring[1])
            for x in range (1,9):
                for y in range (1,4):
                    if self.numMatrix[(x-1)][(zahlzwei*3)+(y-1)]==1:
                        self.pixels[y+4][9-x]=[0,0,255]
                    else:
                        self.pixels[y+4][9-x]=[0,0,0]
            zahldrei = int(timestring[2])
            for x in range (1,9):
                for y in range (1,4):
                    if self.numMatrix[(x-1)][(zahldrei*3)+(y-1)]==1:
                        self.pixels[y+11][9-x]=[0,0,255]
                    else:
                        self.pixels[y+11][9-x]=[0,0,0]
            zahlvier = int(timestring[3])
            for x in range (1,9):
                for y in range (1,4):
                    if self.numMatrix[(x-1)][(zahlvier*3)+(y-1)]==1:
                        self.pixels[y+15][9-x]=[0,0,255]
                    else:
                        self.pixels[y+15][9-x]=[0,0,0]
            #Punkte
            timestring = time.strftime("%S")
            if int(timestring)%2==0:
                self.pixels[9][2]=[0,0,255]
                self.pixels[10][2]=[0,0,255]
                self.pixels[9][3]=[0,0,255]
                self.pixels[10][3]=[0,0,255]
    
                self.pixels[9][6]=[0,0,255]
                self.pixels[10][6]=[0,0,255]
                self.pixels[9][7]=[0,0,255]
                self.pixels[10][7]=[0,0,255]
            else:
                self.pixels[9][2]=[0,0,0]
                self.pixels[10][2]=[0,0,0]
                self.pixels[9][3]=[0,0,0]
                self.pixels[10][3]=[0,0,0]
    
                self.pixels[9][6]=[0,0,0]
                self.pixels[10][6]=[0,0,0]
                self.pixels[9][7]=[0,0,0]
                self.pixels[10][7]=[0,0,0]
            self.drawsnake()
            time.sleep(1)
        print("Time closed")