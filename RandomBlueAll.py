#!/usr/bin/env python
import math, sys
import time, random
import colorsys
import pygame
from pygame.locals import *
from colorsys import hsv_to_rgb, rgb_to_hsv

fromcolor = 0.0
tocolor = 1.0
pixels = [[[0 for x in range(3)] for x in range(10)] for x in range(20)]
brightness = 1.0
waittime = 50
waitbright = 200
waitint = 100
REFRESHSCREEN = USEREVENT+1
spidev = file("/dev/spidev0.0", "wb")
def hsv2rgb(h,s,v):
    return tuple(int(i * 255) for i in colorsys.hsv_to_rgb(h,s,v))
def rgb2hsv(r,g,b):
    return tuple(i  for i in colorsys.rgb_to_hsv(r/ 255.0, g/ 255.0, b/ 255.0))
def draw():
        for row in pixels:
                for pixel in row:
                        for color in pixel:
                                c = int(color*brightness)
                                spidev.write(chr(c & 0xFF))
        spidev.flush()
        time.sleep(0.001)
def initScreen():
    global pixels
    global brightness
    for row in range(0,20):
        for pixel in range(0,10):
            r, g, b = hsv2rgb(random.uniform(fromcolor,tocolor),random.random(),1)
            pixels[row][pixel]=[r,g,b]
    draw()
def changePixels():
    global pixels
    global brightness
    for i in range(0,5):
        row = random.randint(0,19)
        col = random.randint(0,9)
        r, g, b = hsv2rgb(random.uniform(fromcolor,tocolor),random.random(),1)
        pixels[row][col] = [r,g,b]
    draw() 
if __name__ == '__main__':
    fromcolor = float(sys.argv[1])/360
    tocolor = float(sys.argv[2])/360
    pygame.quit()
    print("Random blue pixels")
    pygame.init()
    joystick_count = pygame.joystick.get_count()
    if joystick_count == 0:
        print ("Error, I did not find any joysticks")
    else:
        j = pygame.joystick.Joystick(0)
        j.quit()
        j.init()
        print 'Initialized Joystick : %s' % j.get_name()
    initScreen()
    pygame.time.set_timer(REFRESHSCREEN, waittime)
    cl = pygame.time.Clock()
    start = pygame.time.get_ticks()
    startbright = start
    startint = start
    while 1:
        pygame.event.pump()
        #Check if waitbright-Intervall has passed since last change of brightness and update if buttons pressed
        if (pygame.time.get_ticks()>=startbright+waitbright):
            if j.get_axis(1) <= -0.5:
                if brightness <= 0.95:
                    brightness +=0.05
                    
            if j.get_axis(1) >= +0.5:
                if brightness >= 0.05:
                    brightness -=0.05
            draw()
            startbright = pygame.time.get_ticks()
                        
        if (pygame.time.get_ticks()>=startint+waitint):
            if j.get_axis(0) >= +0.5:
                if waittime <= 9980:
                    waittime +=20
                   
            if j.get_axis(0) <= -0.5:
                if waittime >= 20:
                    waittime -=20
            startint = pygame.time.get_ticks() 

        if j.get_button(1):
            waittime = 1
            brightness = 1.0
            startint = pygame.time.get_ticks()
            changePixels()        
        
        if (pygame.time.get_ticks()>=start+waittime):
                changePixels()
                start = pygame.time.get_ticks()